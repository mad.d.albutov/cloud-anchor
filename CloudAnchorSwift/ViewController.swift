//
//  ViewController.swift
//  CloudAnchorSwift
//
//  Created by Kei Fujikawa on 2018/05/12.
//  Copyright © 2018年 Kboy. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import ARCore
import Firebase

class ViewController: UIViewController {

    @IBOutlet weak var trackingStateLabel: UILabel!
    @IBOutlet weak var debugInfoLabel: UILabel!
    @IBOutlet weak var sceneView: ARSCNView!
    
    private lazy var firebaseDatabase = Database.database().reference().child("hotspot_list")
    private var gSession: GARSession?
    private var fetchedAnchorIds: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureGARSession()
        observeAnchors()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sceneView.session.pause()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let touchLocation = touch.location(in: sceneView)
        let hitTestResults = sceneView.hitTest(
            touchLocation,
            types: [
                .existingPlane,
                .existingPlaneUsingExtent,
                .estimatedHorizontalPlane
            ]
        )
        
        if let result = hitTestResults.first {
            addAnchor(transform: result.worldTransform)
        }
    }
    
    @IBAction func fetchButtonTapped(_ sender: Any) {
        resolveAnchors()
    }
    
    private func setTrackingStateText(_ text: String) {
        trackingStateLabel.text = "State: " + text
    }
    
    private func configureViews() {
        sceneView.delegate = self
        sceneView.session.delegate = self
    }
    
    private func configureGARSession() {
        gSession = try? GARSession(apiKey: "AIzaSyAR02HNMh3-VxHA0UeqQE_Vi4A-3pxfPvU",
                                   bundleIdentifier: nil)
        gSession?.delegate = self
        gSession?.delegateQueue = .main
        
        let configuration = GARSessionConfiguration()
        configuration.cloudAnchorMode = .enabled
        
        var error: NSError? = nil
        gSession?.setConfiguration(configuration, error: &error)
        
        if error != nil {
            print("----Err: \(String(describing: error))")
        }
    }
}

// MARK: - Some Original Method
extension ViewController {
    private func observeAnchors() {
        firebaseDatabase.observe(.value) { [weak self] (snapshot) in
            guard let value = snapshot.value as? [String : Any] else {
                return
            }
            
            value.values.forEach { [weak self] value in
                if let dic = value as? [String: Any],
                    let anchorId = dic["hosted_anchor_id"] as? String {
                    self?.fetchedAnchorIds.append(anchorId)
                }
            }
        }
    }
    
    private func resolveAnchors(){
        debugInfoLabel.text = "Resolving ..."
        fetchedAnchorIds.forEach { id in
            _ = try? gSession?.resolveCloudAnchor(id)
        }
        
        fetchedAnchorIds.removeAll()
    }
    
    private func addAnchor(transform: matrix_float4x4) {
        debugInfoLabel.text = "Hosting ..."
        let arAnchor = ARAnchor(transform: transform)


        sceneView.session.add(anchor: arAnchor)
        print("anchor addded")
        do {
            try gSession?.hostCloudAnchor(arAnchor)
        } catch {
            print(error)
        }
    }
}

// MARK: - ARSCNViewDelegate
extension ViewController: ARSCNViewDelegate {
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        if !(anchor is ARPlaneAnchor) {
//            let scene = SCNScene(named: "art.scnassets/ship.scn")!
//            return scene.rootNode.childNode(withName: "ship", recursively: false)
            let box = SCNBox(width: 0.1, height: 0.2, length: 0.1, chamferRadius: 0)
            let node = SCNNode(geometry: box)
            return node
        }
        return nil
    }
    
}

// MARK: - ARSCNViewDelegate
extension ViewController: ARSessionDelegate {
    
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        do {
            try gSession?.update(frame)
        } catch {
            print(error)
        }
    }
    
    func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
        let text: String
        
        switch camera.trackingState {
            
        case .normal:
            text = "Normal"
            
        case .notAvailable:
            text = "unAvailable"
            
        case .limited(let reason):
            
            switch reason {
            case .excessiveMotion:
                text = "too fast"
                
            case .initializing:
                text = "initializing"
                
            case .insufficientFeatures:
                text = "low features count"
                
            case .relocalizing:
                text = "relocalizing"
            }
            
        }
        
        setTrackingStateText(text)
    }
}

// MARK: - GARSessionDelegate
extension ViewController: GARSessionDelegate {
    
    func session(_ session: GARSession, didHost anchor: GARAnchor) {
        let id = anchor.cloudIdentifier
        firebaseDatabase.childByAutoId().child("hosted_anchor_id").setValue(id)
        debugInfoLabel.text = "Anchor was hosted"
    }
    
    func session(_ session: GARSession, didFailToHost anchor: GARAnchor) {
        debugInfoLabel.text = "Anchor hosting failed"        
    }
    
    func session(_ session: GARSession, didResolve anchor: GARAnchor) {
        let arAnchor = ARAnchor(transform: anchor.transform)
        sceneView.session.add(anchor: arAnchor)
        debugInfoLabel.text = "Anchor was resolved"
    }
    
    func session(_ session: GARSession, didFailToResolve anchor: GARAnchor) {
        debugInfoLabel.text = "Anchor resolving failed"
    }
}

extension matrix_float4x4 {
    func get2DPosition() -> (Float, Float) {
        (columns.3.x, columns.3.z)
    }
}
